# Wrappers

Metakube simplifies greatly the implementation of workflows by adding some logic within
the docker images of each tool. The way this is done is by creating a wrapper image for each Docker image
to use within the workflow.
Fortunately this is very simple to do.
Add <toolName>/0.1.0+/dpl-conf.yaml file in this directory and fill it up with the following information:
````yaml
versions:
  - <version> #or ##VERSION## to use the current version of the project
metakube:
  from: <registry/image>
  type: alpine #debian or alpine
  appScript: <content of MK_APP script>
````
