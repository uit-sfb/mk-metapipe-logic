#!/bin/bash

THIS_PATH="$( cd "$( dirname "$0" )" && pwd )"

NAME=$(basename $1)

cd "$THIS_PATH"
mkdir -p target
cp -r $NAME target/$NAME
cd target/$NAME
HOMEDIR=$(pwd)
FILEPATH=$(find ./ -maxdepth 2 -type f | grep 'dlp-conf')
sed -i -e "s+##VERSION##+$VERSION+g" $(pwd)${FILEPATH:1}
$THIS_PATH/../docker-wrapping/bin/docker-wrapping -- mk-release --force --registry $REGISTRY .