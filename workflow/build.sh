#!/usr/bin/env bash

set -e

#note: Requires:
# - helm v3.1 or above
# - helm push plugin v0.8.1 or above

POSITIONAL=()
while [[ $# -gt 0 ]]
do
  case "$1" in
    -h|--help)
      echo "Build chart"
      echo ""
      echo "Usage"
      echo "[-h|--help]"
      echo "print this help message"
      exit 0
      ;;
    *) #Unknown
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

THIS_PATH="$( cd "$( dirname "$0" )" && pwd )"
ORIGIN=$(pwd)

VERSION=$(cat "$ORIGIN/.version")
echo "WORKFLOW_VERSION=$VERSION"
echo "METAKUBE_VERSION=$METAKUBE_VERSION"
CHART_VERSION=$VERSION
#Need an exisiting context otherwise helm crashes
kubectl config set-context build
kubectl config use-context build
#Install Helm push plugin
helm plugin install https://github.com/chartmuseum/helm-push 2> /dev/null || true
#Add stable to repo list
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
#Add chartmuseum to your repo list
helm repo add chartmuseum https://chartmuseum.metapipe.uit.no
WORKFLOW_NAME="$(yq r $THIS_PATH/workflow.yaml name || echo "")"
CHART_NAME="$(echo $WORKFLOW_NAME | tr -cd '[:alnum:]_-' | awk '{print tolower($0)}')"
WORKFLOW_DESCRIPTION="$(yq r $THIS_PATH/workflow.yaml description || echo "")"
WORKFLOW_DEFAULT_PATHWAY="$(yq r $THIS_PATH/workflow.yaml defaultPathway || echo "")"
WORKFLOW_DEFAULT_PROFILE="$(yq r $THIS_PATH/workflow.yaml defaultProfile || echo "")"
WORKFLOW_LOGO_URL="$(yq r $THIS_PATH/workflow.yaml assets.logoUrl || echo "")"
WORKFLOW_PIC_URL="$(yq r $THIS_PATH/workflow.yaml assets.workflowPicUrl || echo "")"
#METAKUBE_VERSION is already set via metakubeVersion
#METAKUBE_VERSION=$(yq r $THIS_PATH/workflow.yaml metakubeVersion)
DIR="$THIS_PATH/target"
helm repo update
rm -rf "$DIR"
mkdir -p "$DIR"
cd $DIR
echo "https://artifactory.metapipe.uit.no/artifactory/generic-local/no.uit.sfb/metakube/${METAKUBE_VERSION}/helm.zip"
curl "https://artifactory.metapipe.uit.no/artifactory/generic-local/no.uit.sfb/metakube/${METAKUBE_VERSION}/helm.zip" --output helm.zip
unzip -q helm.zip && rm helm.zip
find "$DIR/metakube" -maxdepth 2 -type f -exec awk -i inplace "{gsub(\"{{WORKFLOW_VERSION}}\", \""$CHART_VERSION"\", \$0); print}" {} \;
find "$DIR/metakube" -maxdepth 2 -type f -exec awk -i inplace "{gsub(\"{{CHART_NAME}}\", \""$CHART_NAME"\", \$0); print}" {} \;
echo "    app.name = \"$WORKFLOW_NAME\"" >> metakube/templates/backend-conf.yaml
echo "app.pipeline.description = \"\"\"$WORKFLOW_DESCRIPTION\"\"\"" | sed -e 's/^/    /' >> metakube/templates/backend-conf.yaml
echo "    app.pipeline.defaultProfile = \"$WORKFLOW_DEFAULT_PROFILE\"" >> metakube/templates/backend-conf.yaml
echo "    app.pipeline.defaultPathway = \"$WORKFLOW_DEFAULT_PATHWAY\"" >> metakube/templates/backend-conf.yaml
echo "    app.pipeline.assets.logoUrl = \"$WORKFLOW_LOGO_URL\"" >> metakube/templates/backend-conf.yaml
echo "    app.pipeline.assets.workflowPicUrl = \"$WORKFLOW_PIC_URL\"" >> metakube/templates/backend-conf.yaml
mkdir $DIR/metakube/pipeline
cp -r ../templates $DIR/metakube/pipeline
cp -r ../profiles $DIR/metakube/pipeline
cp -r ../pathways $DIR/metakube/pipeline
find "$DIR/metakube/pipeline/templates" -maxdepth 2 -type f -exec awk -i inplace "{gsub(\"##VERSION##\", \""$VERSION"\", \$0); print}" {} \;
helm lint "$DIR/metakube"
#--app-version must be METAKUBE_VERSION
helm package --version $CHART_VERSION -d $DIR --app-version $METAKUBE_VERSION -u --debug "$DIR/metakube"
helm template "$DIR/${CHART_NAME}-${CHART_VERSION}.tgz"
helm push -u $CHARTMUSEUM_USER -p $CHARTMUSEUM_PASSWORD -f "$DIR/${CHART_NAME}-${CHART_VERSION}.tgz" chartmuseum
