# META-pipe

[META-pipe](https://https://gitlab.com/uit-sfb/mk-metapipe-logic) is a workflow for analysis and annotation of metagenomic samples,
providing insight into phylogenetic diversity, as well as metabolic and functional properties of environmental communities.

As illustrated in the diagram below, the workflow is divided into four modules:
  - filtering and assembly
  - taxonomic classification
  - functional assignment
  - binning

![META-pipe workflow](source/images/schematics/Agnostic_METApipe_bioinf_pipeline.png)  
*META-pipe workflow schematic*

The workflow was [designed](https://munin.uit.no/handle/10037/11180) to leverage the most modern tools to 
ensure both quality outputs and efficient resource usage.

In addition, it is now clear that the quality of the reference databases plays a role at least as important
as the tools themselves when it comes to output quality and execution speed.
This is why META-pipe makes use of *plugged-in* reference databases gathered into profiles.
For instance, the *Marine* profile uses exclusively the high quality [MAR databases](https://mmp.sfb.uit.no/databases/) as reference databases.

<div align="center">
  ![META-pipe profiles](source/images/schematics/profiles.png)  
</div>
*META-pipe profiles*

## Workflow as a service

META-pipe is implemented as a [METAkube](https://gitlab.com/uit-sfb/metakube) workflow.
As such, the workflow can be easily deployed on any Kubernetes cluster and provided as a services for
a local or global scientific community thanks to an easy to use web interface.

## Getting started

Follow the steps below and it should not take more than 5 minutes to set-up META-pipe!
Simply download the [installation script](https://artifactory.metapipe.uit.no/artifactory/generic-local/no.uit.sfb/metapipe/1.0.0-profiles-SNAPSHOT/metapipe-deployment.zip), unzip it
and follow the instructions listed in the `README.md` file.

